<?php
    error_reporting (E_ALL ^ E_NOTICE);
    $a = $_GET["1"]; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Css -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <title>Profile</title>
</head>
<body>
    <!-- MENU -->
    <nav class="navbar navbar-expand navbar-light bg-light border-bottom">
        <a class="navbar-brand" href="Process.html">
            <i class="fas fa-home"></i>
        </a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="Process.html">PROCESSES</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Template.html">TEMPLATES</a>
            </li>
        </ul>
        <div class="nav-item dropdown">
            <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="img-small-menu" src="/assets/img/logo.jpg" alt="Avatar">
                <span style="margin-left: 12px">Username</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="Profile.html">Profile Setting</a>
                <a class="dropdown-item" href="WebSetting.html">Web Setting</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="Login.html">Sign Out</a>
            </div>
        </div>
    </nav>

    <!-- Body Part -->
    <div class="wrapper_main">
        <!-- ส่วนหัว -->
        <!-- เก็บหัวข้อ Tab -->
        <header class="content-head profile box-one">
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="btn btn-normal <?php if($a != "1"){ echo 'active'; } ?> " id="pills-GeneralSettings-tab" data-toggle="pill" href="#pills-GeneralSettings" role="tab" aria-controls="pills-GeneralSettings" aria-selected="true">
                        <i class="far fa-user"></i> <span>General Settings</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-normal" id="pills-Notifications-tab" data-toggle="pill" href="#pills-Notifications" role="tab" aria-controls="pills-Notifications" aria-selected="false">
                        <i class="far fa-bell"></i> <span>Notifications</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-normal <?php if($a == "1"){ echo 'active'; }?>" id="pills-Password-tab" data-toggle="pill" href="#pills-Password" role="tab" aria-controls="pills-Password" aria-selected="false">
                        <i class="fas fa-lock"></i> <span>Password</span>
                    </a>
                </li>
            </ul>
        </header>

        <!-- ส่วนตัว -->
        <div class="content-body-profile">
            <div id="pills-tabContent" class="tab-content">
                <!-- General Set -->
                <div class="tab-pane fade <?php if($a != "1"){ echo 'show active'; } ?>" id="pills-GeneralSettings" role="tabpanel" aria-labelledby="pills-GeneralSettings-tab">
                    <!-- รูป -->
                    <div style="display: flex">
                        <!-- ส่วนของรูปภาพ -->
                        <div style="margin-right: 12px">
                            <!-- กรณียังไม่อัพรูป  -->
                            <div id="defualtAvatarUpload" class="defualt-avatar-upload"></div>
                            <!-- กรณีที่อัพรูปแล้ว -->
                            <img id="picUpload" class="avatar-upload" src="#" alt="Avatar">
                        </div>
                        <!-- กล่องด้านข้างรูป avatar -->
                        <div class="box-side-avatar">
                            <!-- ส่วนอัพโหลดภาพ -->
                            <!-- ถ้ายังไม่เคยอัพภาพ จะแสดงส่วนนี้ -->
                            <div id="up-img">
                                <input type="file" name="photo" id="upload-photo" onchange="readURL(this);" accept="image/*" />
                                <label for="upload-photo">Upload photo</label>
                            </div>
                            <!-- ส่วนเปลี่ยนภาพ และ ลบภาพ -->
                            <!-- กรณีเคยอัพรูปภาพแล้ว -->
                            <div id="change-clear-photo" class="change-photo">
                                <input type="file" name="photo" id="changephoto" onchange="readURL(this);" accept="image/*" />
                                <label for="changephoto">Change photo</label>
                                <p id="ClearPhoto">Clear photo</p>
                            </div>

                            <p class="discri-photo">Profile photos must be JPGs or PNGs up to 5MB</p>
                        </div>
                    </div>
                    <!-- Form -->
                    <!-- Form :: Firstname -->
                    <div class="profile-GeneralSet-form" class="profile-GeneralSet-form">
                        <div class="profile-GeneralSet-form-label">
                            <form>
                                <label  for="Firstname">First name</label>
                            </form>
                        </div>
                        <div>
                            <form>
                                <input class="form-control form-control-sm" type="text">
                            </form>
                        </div>
                    </div>
                    <!-- form :: Lastname -->
                    <div class="profile-GeneralSet-form">
                        <div class="profile-GeneralSet-form-label">
                            <form>
                                <label  for="Last name">Last name</label>
                            </form>
                        </div>
                        <div>
                            <form>
                                <input class="form-control form-control-sm" type="text">
                            </form>
                        </div>
                    </div>
                    <!-- Email -->
                    <div class="profile-GeneralSet-form">
                        <div class="profile-GeneralSet-form-label">
                            <form>
                                <label for="Email">Email</label>
                            </form>
                        </div>
                        <div>
                            <form>
                                <input class="form-control form-control-sm" type="text">
                            </form>
                        </div>
                    </div>
                    <!-- Time Zone -->
                    <!-- ยังไม่ได้ใส่ข้อมูลลงไป -->

                    <!-- Member of กลุ่มที่อยู่-->
                    <div class="profile-GeneralSet-form">
                        <div class="profile-GeneralSet-form-label">
                            <form>
                                <label for="MemberOf">Member of:</label>
                            </form>
                        </div>
                        <div>
                            <form>
                                <label style="margin: 6px 0 0 0" for="Last name" >
                                    Accounting, HR, IT, Managers, Marketing, Sales
                                </label>
                            </form>
                        </div>
                    </div>
                    <!-- Check box :: Show help tooltips -->
                  
                    <!-- ปุ่ม save change -->
                    <div class="profile-GeneralSet-form">
                        <div class="profile-GeneralSet-form-label">
                        </div>
                        <div>
                            <button class="btn btn-green">
                                <span>Save Change</span>
                            </botton>
                        </div>
                    </div>
                </div>
                <!-- Notification -->
                <div class="tab-pane fade" id="pills-Notifications" role="tabpanel" aria-labelledby="pills-Notifications-tab">
                    <p style="margin-bottom: 0">
                        Select when we should send notifications to your e–mail address (<span><b>Exampleemail@hotmail.com</b></span>)
                        <!-- ****** ปัญหา 1. ปุ่ม check all กับ Uncheck มีปัญหาคือ ถ้าไปยุ่งกับcheckboxตัวใดตัวนึง มันจะไม่สนใจตัวที่เรายุ่งอีกเลย-->
                        <a id="Check-all">Check all</a> • <a id="Uncheck-all">Uncheck all</a>
                        <!-- ****** ปัญหา 2. status save auto เวลาที่ check หรือ Uncheck มันจะขึ้นมาแปปนึง แล้วหายไป -->
                        <!-- <span>saved!!</span> -->
                    </p>
                    <!-- รายการสำหรับ checkทั้งหมด -->
                    <div class="container-fluid list-check">
                        <div class="row">
                            <!-- ข้างซ้าย -->
                            <div class="col-6 list-check-box">
                                <!-- check list -->
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck0">
                                    <label class="custom-control-label" for="customCheck0">New available tasks</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                                    <label class="custom-control-label" for="customCheck2">You got new tasks</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck3">
                                    <label class="custom-control-label" for="customCheck3">New comment from someone on your task</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck4">
                                    <label class="custom-control-label" for="customCheck4">Someone mentioned you in comment</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck5">
                                    <label class="custom-control-label" for="customCheck5">Task is deleted</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck6">
                                    <label class="custom-control-label" for="customCheck6">Task is completed</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck7">
                                    <label class="custom-control-label" for="customCheck7">Completed task is reopened</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck8">
                                    <label class="custom-control-label" for="customCheck8">Your task was reopened by someone</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck9">
                                    <label class="custom-control-label" for="customCheck9">Your task is completed by someone</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck10">
                                    <label class="custom-control-label" for="customCheck10">Some tasks are not available to anyone</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck11">
                                    <label class="custom-control-label" for="customCheck11">Some tasks unassigned in your process</label>
                                </div>
                            </div>
                            <!-- ข้างขวา -->
                            <div class="col-6 list-check-box">
                                <!-- check list -->
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck12">
                                    <label class="custom-control-label" for="customCheck12">Some processes are not managed by anyones</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck13">
                                    <label class="custom-control-label" for="customCheck13">Your process is deleted</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck14">
                                    <label class="custom-control-label" for="customCheck14">Your process is completed</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck15">
                                    <label class="custom-control-label" for="customCheck15">Your process is rejected</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck16">
                                    <label class="custom-control-label" for="customCheck16">Your tasks were assigned</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck17">
                                    <label class="custom-control-label" for="customCheck17">New process started</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck18">
                                    <label class="custom-control-label" for="customCheck18">New comment from someone in your process</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck19">
                                    <label class="custom-control-label" for="customCheck19">You started new process</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck20">
                                    <label class="custom-control-label" for="customCheck21">Your tasks are due today</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck21">
                                    <label class="custom-control-label" for="customCheck21">Some templates are not managed by anyone</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Password -->
                <div class="tab-pane fade <?php if($a == "1"){ echo 'show active'; }?>" id="pills-Password" role="tabpanel" aria-labelledby="pills-Password-tab">
                    <!-- Form :: New password -->
                    <div class="profile-password-form">
                        <div class="profile-password-form-label">
                            <form>
                                <label for="Newpassword">New password</label>
                            </form>
                        </div>
                        <div>
                            <form id="box-newPass">
                                <input id="new-pass" class="form-control form-control-sm" type="text">
                                <!-- จะแสดงขึ้นมาหาก pass ที่ใส่ไม่ตรงกับที่กำหนด -->
                                <span id="message-newPass" class="error-text">Minimum of 8 characters</span>
                                <span id="message-required" class="error-text">Required</span>
                            </form>
                        </div>
                    </div>
                    <!-- form :: Repeat new password -->
                    <div class="profile-password-form">
                        <div class="profile-password-form-label">
                            <form>
                                <label for="Repeatnewpassword">Repeat new password</label>
                            </form>
                        </div>
                        <div>
                            <form id="box-rePass">
                                <input id="re-pass" class="form-control form-control-sm" type="text">
                                <!-- จะแสดงขึ้นมาหาก pass ที่ใส่ไม่ตรงกับที่กำหนด -->
                                <span id="message-shouldMatch" class="error-text">Passwords should match</span>
                            </form>
                        </div>
                    </div>
                    <!-- form :: ปุ่ม  Change password-->
                    <div class="profile-password-form">
                        <div class="profile-password-form-label">
                        </div>
                        <div>
                            <button id="changepassword" class="btn btn-unclick" disabled>
                                <span>Change password</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Script -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/js/event.js"></script>
</body>
</html>
