//Process Detail :: Deadline มีตัวเดียว
function deadline(){
    var x = document.getElementById("ProcessDeadline-select").value;
    var y = 'Notset';
    var z = 'Afterprocessstart';
    if(x == y){
        document.getElementById("ProcessDeadline-day").disabled = true;
        document.getElementById("ProcessDeadline-chocie").disabled = true;
    }else if(x == z){
        document.getElementById("ProcessDeadline-day").disabled = false;
        document.getElementById("ProcessDeadline-chocie").disabled = false;
    }
}
// Task+Approval Detail ::  Due date ใช้ได้กับหลายๆตัว
$('.duedatechange').on("change",function(){
    var x = $(this).val();
    var y = 'Not set';
    var z = 'After process start';
    var a = 'After previous task complete';
    var b = 'Before process deadline';
    console.log($(this).next().children());
    var statusofchild = $(this).next().children();
    if(x == y){
        statusofchild.attr('disabled','disabled');
    }else if(x == z || x == a || x == b){
        statusofchild.removeAttr('disabled');
    }
});

/* Tab :: condition all item */
//Condition :: Task+app+group
$('.Conditionchoice').on("change",function(){
    var x = $(this).val();
    var y = 'Always';
    var z = 'If All conditions are met';
    var a = 'If Any conditions is met';
    // อ้างถึง box-condition
    var boxcondition = $(this).next();
    if(x == y){
        boxcondition.css('display','none');
    }else if(x == z){
        boxcondition.css('display','block');
        $('.conditionTag').css('display','block');
        $('.conditionTag').text("AND");
    }else if(x == a){
        boxcondition.css('display','block');
        $('.conditionTag').css('display','block');
        $('.conditionTag').text("OR");
    }
});


//select หน้า process
function statusprocess(){
    var processstate = document.getElementById("processStatus").value;
    var groupstatus = document.getElementById("groupStatus").value;
    var status = document.getElementById("Status").value;
    //Myprocess Nogroup Active
    if(processstate == 1 && groupstatus == 3 && status == 5){
        document.getElementById("boxA").style.display ="block";
        document.getElementById("boxB").style.display ="none";
        document.getElementById("boxC").style.display ="none";
        document.getElementById("boxD").style.display ="none";
        document.getElementById("boxE").style.display ="none";
    // Myprocess Nogroup completed  / Allprocess Nogroup Completed 
    }else if((processstate == 1 || processstate == 2) && groupstatus == 3 && status == 6){
        document.getElementById("boxA").style.display ="none";
        document.getElementById("boxB").style.display ="block";
        document.getElementById("boxC").style.display ="none";
        document.getElementById("boxD").style.display ="none";
        document.getElementById("boxE").style.display ="none";
    //Allprocess Nogroup Active 
    }else if(processstate == 2 && groupstatus == 3 && status == 5){
        document.getElementById("boxA").style.display ="none";
        document.getElementById("boxB").style.display ="none";
        document.getElementById("boxC").style.display ="block";
        document.getElementById("boxD").style.display ="none";
        document.getElementById("boxE").style.display ="none";
    //Myprocess Group Active / All process Group Active
    }else if((processstate == 1 || processstate == 2) && groupstatus == 4 && status == 5){
        document.getElementById("boxA").style.display ="none";
        document.getElementById("boxB").style.display ="none";
        document.getElementById("boxC").style.display ="none";
        document.getElementById("boxD").style.display ="block";
        document.getElementById("boxE").style.display ="none";
    //Myprocess Group Complated  / Allprocess Group Complated 
    }else if((processstate == 1 || processstate == 2) && groupstatus == 4 && status == 6){
        document.getElementById("boxA").style.display ="none";
        document.getElementById("boxB").style.display ="none";
        document.getElementById("boxC").style.display ="none";
        document.getElementById("boxD").style.display ="none";
        document.getElementById("boxE").style.display ="block";
    }
}

//select หน้า Template 
$('.chosen').chosen({
    width: '200px',
    allow_single_deselect: true,
});

//select หน้า Creattemplate
//start start Processmanagers
$('.chosen-select').chosen({
    width: '100%'
});

function test(){
    var x = document.getElementById('testuservalue').value;
    console.log(x);
}

//สำหรับการเพิ่มลดกลุ่มของสมาชิกนั้นๆ
$('.addGroupToUser').on('click',function(){
    $(this).parent().next().css('display','block');
})
$('.BtnAddgroup').on('click',function(){
    // เก็บค่าตัว select ที่เราเลือกไว้
    var selectValue = $(this).prev().val();
    //อ้างไปที่ group list
    var groupList = $(this).parent().next();
    //ตัวแรก
    var count = groupList.children().length;
    groupList.append('<li class="box-one" style="margin-bottom: 8px;">'+
        '<div class="icon-small-size" style="background: #E8E8E8; margin-right: 5px">'+
        '<i class="fas fa-users" style="color: #959595"></i></div>'+
        '<span>'+selectValue+'</span></li>');
});

$('.BtnCancelgroup').on('click',function(){
    //เมื่อกด cancel ตัวมันเองจะถูกซ่อน
    $(this).parent().css('display','none');
    //เช็คก่อนว่ามีกลุ่มใน ul มี liหรือไม่
    var count = $(this).parent().next().children().length;
    console.log(count);
    //ถ้าไม่มี count=0 ให้แสดง isn't a member of any group + add 
    var nogroup = "isn't a member of any group";
    var havegroup = "is a member of "+ count +" group";
    var textDisplay = $(this).parent().prev().children();
    if(count == 0){
        textDisplay.eq(0).text(nogroup);
        textDisplay.eq(1).text('Add');
    }
    //ถ้ามี count>0 ให้แสดง is a member of count group + edit
    else{
        textDisplay.eq(0).text(havegroup);
        textDisplay.eq(1).text('Edit');
    }
});

//สำหรับเพิ่มลดสมาชิกของกลุ่ม
$('.addUserToGroup').on('click',function(){
    $(this).parent().next().css('display','block');
})
$('.BtnAdduser').on('click',function(){
    // เก็บค่าตัว select ที่เราเลือกไว้
    var selectValue = $(this).prev().val();
    //อ้างไปที่ group list
    var groupList = $(this).parent().next();
    //ตัวแรก
    var count = groupList.children().length;
        groupList.append('<li class="box-one" style="margin-bottom: 8px;">'+
            '<div class="img-small" style="background: #B8E17D; margin-right: 5px">'+
            '<p>O</p></div>'+
            '<span>'+selectValue+'</span></li>');
});

$('.BtnCanceluser').on('click',function(){
    //เมื่อกด cancel ตัวมันเองจะถูกซ่อน
    $(this).parent().css('display','none');
    //เช็คก่อนว่ามีกลุ่มใน ul มี liหรือไม่
    var count = $(this).parent().next().children().length;
    console.log(count);
    //ถ้าไม่มี count=0 ให้แสดง isn't a member of any group + add 
    var nogroup = "There are no users in this group";
    var havegroup = "This group includes "+ count +" user";
    var textDisplay = $(this).parent().prev().children();
    if(count == 0){
        textDisplay.eq(0).text(nogroup);
        textDisplay.eq(1).text('Add');
    }
    //ถ้ามี count>0 ให้แสดง is a member of count group + edit
    else{
        textDisplay.eq(0).text(havegroup);
        textDisplay.eq(1).text('Edit');
    }
});